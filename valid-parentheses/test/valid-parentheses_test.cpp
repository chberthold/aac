// Entetes //---------------------------------------------------------------------------------------
#include "catch.hpp"

#include <sstream>
#include <string>
#include <typeinfo>

#include "ValidParentheses.hpp"

// Tests //-----------------------------------------------------------------------------------------

ValidParentheses vp;

//------------------------------------------------------------------------------------------------ 1
TEST_CASE ( "ValidParentheses::ValidSimpleCases" ) {
 REQUIRE ( vp.isValid("()") == true );
 REQUIRE ( vp.isValid("{}") == true );
 REQUIRE ( vp.isValid("[]") == true );
 REQUIRE ( vp.isValid("") == true );
}

TEST_CASE ( "ValidParentheses::ValidNormalCases" ) {
 REQUIRE ( vp.isValid("([])") == true );
 REQUIRE ( vp.isValid("[{[][]}]") == true );
 REQUIRE ( vp.isValid("{()()}()") == true );
}

TEST_CASE ( "ValidParentheses::InvalidSimpleCases" ) {
 REQUIRE ( vp.isValid("(") == false );
 REQUIRE ( vp.isValid("]") == false );
 REQUIRE ( vp.isValid("(()") == false );
 REQUIRE ( vp.isValid("(}") == false );
 REQUIRE ( vp.isValid("[}") == false );
 REQUIRE ( vp.isValid("(]") == false );
 REQUIRE ( vp.isValid("{]") == false );
 REQUIRE ( vp.isValid("[)") == false );
 REQUIRE ( vp.isValid("{)") == false );
}

TEST_CASE ( "ValidParentheses::InvalidNormalCases" ) {
 REQUIRE ( vp.isValid("()([}{])") == false );
 REQUIRE ( vp.isValid("[(()())([)]{}") == false );
 REQUIRE ( vp.isValid("{[[](())][]]}") == false );
}

// Fin //-------------------------------------------------------------------------------------------
