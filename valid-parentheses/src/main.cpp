#include <iostream>
#include "ValidParentheses.hpp"

int main()
{
    ValidParentheses vp;

    std::cout << "Example 1:\n Input: s = \"()\" \n Output: " << vp.isValid("()") << std::endl;
    std::cout << "Example 2:\n Input: s = \"()[]{}\" \n Output: " << vp.isValid("()[]{}") << std::endl;
    std::cout << "Example 3:\n Input: s = \"(]\" \n Output: " << vp.isValid("(]") << std::endl;

    return 0;
}
