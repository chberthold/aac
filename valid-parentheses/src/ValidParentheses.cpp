#include "ValidParentheses.hpp"

bool ValidParentheses::isValid(std::string s) const
{
    // Create an empty stack to store opening brackets
    std::stack<char> st;

    // Loop through each character in the string
    for (char c : s) {
        // Opening bracket
        if (c == '(' || c == '{' || c == '[') {
            // Push it onto the stack
            st.push(c);
        } else {
            // The stack is empty or closing bracket doesn't match the corresponding opening bracket at the top of the stack
            if (st.empty() ||
                (c == ')' && st.top() != '(') ||
                (c == '}' && st.top() != '{') ||
                (c == ']' && st.top() != '[')) {
                // The string is not valid
                return false;
            }
            // Pop the opening bracket from the stack
            st.pop();
        }
    }
    return st.empty();
}
