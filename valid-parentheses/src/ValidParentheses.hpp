// Guardians
#ifndef _VALID_PARENTHESES_HPP_
#define _VALID_PARENTHESES_HPP_

// Headers
#include <string>
#include <stack>

// Classe
class ValidParentheses
{
public:
    bool isValid(std::string) const;
};

// End
#endif
