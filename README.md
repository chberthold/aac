# Algorithmie avancée et complexité

https://perso.isima.fr/~dahill/AAC/

Deux rendus : choisir deux algos et les dev, commenter dans un doc ceux de l'autre

## Chloé

### huffman

huffman/
```bash
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./huffman
```

### points2

points2/
```bash
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./src/point2-exec
```

## Antoine

### chemin-eulerien

chemin-eulerien/
```bash
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./chemin-eulerien
```

### valid-parentheses

valid-parentheses/
```bash
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./valid-parentheses
```
