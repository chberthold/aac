#ifndef POINT_H
#define POINT_H

#include <string>

class Point{
    private:
        double x;
        double y;
    public:
        Point(double, double);
        Point(){};
        double getX() const{return x;};
        double getY() const{return y;};
        std::string toString() const;
};

/****************************************
 * Compare two points by their abscissa *
 ****************************************/
class CompareX{
    public:
        bool operator()(Point p1, Point p2) const{
            return p1.getX() <= p2.getX();
        };
};

/****************************************
 * Compare two points by their ordinate *
 ****************************************/
class CompareY{
    public:
        bool operator()(Point p1, Point p2) const{
            return p1.getY() <= p2.getY();
        };
};

#endif