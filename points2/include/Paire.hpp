#ifndef PAIRE_H
#define PAIRE_H
#include "Point.hpp"

class Paire{
    private:
        Point p1;
        Point p2;
        double distance;
    public:
        Paire(Point p1, Point p2, double dist) : p1(p1), p2(p2), distance(dist){};
        Paire(){};
        const Point& getP1() const { return p1; };
        const Point& getP2() const { return p2; };
        double getDistance() const{ return distance;};
        void setDistance(double d) {distance = d;};
        void setP1(Point& p){ p1 = p; }
        void setP2(Point& p){ p2 = p; };
};

#endif