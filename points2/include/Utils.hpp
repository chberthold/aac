#ifndef UTILS_H
#define UTILS_H

#include "Point.hpp"
#include "Paire.hpp"
#include <ostream>
#include <vector>
#include <cmath>

using vectPoint = std::vector<Point>;
using vectorIter = std::vector<Point>::const_iterator;

// Print the vector given in the ostream
void printVector(const vectPoint&, std::ostream &);

// Compute the distance beetween two points
double calcul_distance(const Point& p1, const Point& p2);

// Returns the two nearest points amongst the given three
Paire nearest(const Point& p1, const Point& p2, const Point& p3);

// Recursive function that divides the vector organized by abscissa in two
Paire divide(vectorIter beginX, vectorIter endX, const vectPoint& Ty);

#endif