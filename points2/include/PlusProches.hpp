#ifndef PLUSPROCHES_H
#define PLUSPROCHES_H

#include "Point.hpp"
#include "Utils.hpp"
#include <ostream>
#include <set>
#include <tuple>
#include <vector>

class PlusProches{
    private:
        vectPoint Tx;
        vectPoint Ty;
        std::tuple<Point*> nearest;
    public:
        ~PlusProches();
        PlusProches() = default;
        void addPoint(double x, double y);
        vectPoint getTx() const{return Tx;};
        vectPoint getTy() const{return Ty;};
        void printTx(std::ostream & o) const;
        void printTy(std::ostream & o) const;
        void search();
        void solve() const;
        void readFile(const std::string& filename);
};

#endif