# Build

## Debug and Test

```bash
$ mkdir -p build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Debug ../
$ cmake --build .
$ ctest
```

To see Google Tests' verbose output, do

```bash
$ ctest -V
```

# Release
```bash
$ mkdir -p build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ../
$ cmake --build .
```

# Install

```bash
$ cd build
$ cmake --build . --clean-first --target install
```


## How to use :

Needs a file with a list of points (x y). Specify it in the command line : `./src/points2 path/of/file`.

The program will find the two closest points in the list and print both of them and the euclidian distance between them