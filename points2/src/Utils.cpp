#include "../include/Utils.hpp"
#include <vector>
#include <iostream>

/******************************************
 | Function printVector                   |
 |                                        |
 | Write the vector vect in the ostream   |
 ******************************************/
void printVector(const vectPoint & vect, std::ostream & o){
    for(vectorIter it = vect.begin(); it != vect.end(); it++){
        o << (*it).toString() << std::endl;
    }
}

/******************************************
 | Function calcul_distance               |
 |                                        |
 | Calculates the euclidian distance      |
 | between two points                     |
 ******************************************/
double calcul_distance(const Point& p1, const Point& p2){
    return std::sqrt(std::pow(p1.getX() - p2.getX(), 2) + std::pow(p1.getY() - p2.getY(), 2));
}

/******************************************
 | Function nearest                       |
 |                                        |
 | Returns the two nearest points in the  |
 | three provided                         |
 ******************************************/
Paire nearest(const Point& p1, const Point& p2, const Point& p3){
    double dP1P2 = calcul_distance(p1, p2);
    double dP1P3 = calcul_distance(p1, p3);
    double dP2P3 = calcul_distance(p2, p3);

    if(dP1P2 > dP1P3){
        if (dP1P2 > dP2P3){
            return Paire(p1, p2, dP1P2);
        }
        return Paire(p2, p3, dP2P3);
    }
    else{
        if(dP1P3 > dP2P3){
            return Paire(p1, p3, dP1P3);
        }
        return Paire(p2, p3, dP2P3);
    }
}

/******************************************
 | Function nearest                       |
 |                                        |
 | Returns the two points that have the   |
 | minimum distance                       |
 ******************************************/
Paire& nearest(Paire& p1, Paire& p2){
    if(p1.getDistance() < p2.getDistance()){
        return p1;
    }
    return p2;
}

/******************************************
 | Function combine                       |
 |                                        |
 | Final part of the algorithm,           |
 | search in the vector which points are  |
 | in the area between the middle         |
 | and compare the points on each side    |
 | to find the closest ones               |
 ******************************************/
Paire combine(const vectPoint& Ty, Paire& near, double middle){
    vectPoint inArea;
    vectorIter it = Ty.begin();
    vectorIter itNext;
    int point;
    Paire minimum = near;
    double dist;

    /* Get all points in the area surrounding the middle part */
    for(; it != Ty.end(); it++){
        if(fabs((*it).getX() - middle) < near.getDistance()){
            inArea.push_back(*it);
        }
    }

    /* Try to find the closest points in the list */
    for(it = inArea.begin(); it != inArea.end(); it++){
        point = 0;
        itNext = it + 1;

        /* Get distances for the 7 next points */
        while(itNext != inArea.end() && (*it).getY() - (*itNext).getY() < minimum.getDistance()){
            dist = calcul_distance(*it, *itNext);

            /* If distance is the minimum, save it */
            if(minimum.getDistance() > dist){
                minimum = Paire(*it, *itNext, dist);
            }
            itNext ++;
            point ++;
        }
    }

    return (minimum.getDistance() < near.getDistance() ? minimum : near);
}

/******************************************
 | Function divide                        |
 |                                        |
 | First part of the algorithm,           |
 | divide the points in two and           |
 | recursively solve the problem for each |
 | sub-vector of points. If there are only|
 | three or less points, solve            |
 | sequentially by comparing the distances|
 ******************************************/
Paire divide(vectorIter beginX, vectorIter endX, const vectPoint& Ty){
    int length = endX - beginX;
    int middlePoint;
    double middleDist;
    vectPoint TyLeft;
    vectPoint TyRight;

    /* If there are more than three points in the list */
    if(length > 3){
        middlePoint = length/2;
        middleDist = ((*(beginX + middlePoint - 1)).getX() + (*(beginX + middlePoint)).getX()) / 2;

        /* Separate the points in vectors sorted by y */
        for (vectorIter it = Ty.begin(); it != Ty.end(); it++) {

            /* Points on the sleft side of the middle */
            if ((*it).getX() <= middleDist)
                TyLeft.push_back(*it);

            /* Points on the right side of the middle */
            else
                TyRight.push_back(*it);
        }

        Paire left = divide(beginX, beginX + middlePoint, TyLeft);
        Paire right = divide(beginX + middlePoint, endX, TyRight);
        return combine(Ty, nearest(left, right), middleDist);
    }

    /* If there are only three points, compare the distances */
    else if(length == 3){
        return nearest(*beginX, *(beginX+1), *(beginX+2));
    }

    /* If there are only two points, just return them, they are the closest */
    else {
        return Paire(*beginX, *(beginX+1), calcul_distance(*beginX, *(beginX+1)));
    }

}