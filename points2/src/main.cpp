#include "../include/PlusProches.hpp"
#include <iostream>
#include <fstream>
#include <cstring>


int main(int argc, char *argv[]){
    PlusProches points;

    if (argc == 2) {
        std::string filename = argv[1];
        points.readFile(filename);
        points.printTx(std::cout);
        points.printTy(std::cout);
        points.search();
    }
    else{
        std::cout << "Please, specify the file name in the command line" << std::endl;
    }

    return 0;
}