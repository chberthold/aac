#include "../include/PlusProches.hpp"
#include "../include/Utils.hpp"
#include <iostream>
#include <utility>
#include <vector>
#include <cmath>
#include <fstream>
#include <algorithm>

/******************************************
 | Function printTx                       |
 |                                        |
 | Write the vector Tx in the ostream     |
 ******************************************/
void PlusProches::printTx(std::ostream & o) const{
    o << "Tx (sorted by x) :" << std::endl;
    printVector(Tx, o);
}

/******************************************
 | Function printTy                       |
 |                                        |
 | Write the vector Ty in the ostream     |
 ******************************************/
void PlusProches::printTy(std::ostream & o) const{
    o << "Ty (sorted by y) :" << std::endl;
    printVector(Ty, o);
}

PlusProches::~PlusProches(){
    Tx.clear();
    Ty.clear();
}

/******************************************
 | Function addPoint                      |
 |                                        |
 | Add a point in the two vectors         |
 ******************************************/
void PlusProches::addPoint(double x, double y){
    Point p(x, y);
    Tx.push_back(p);
    Ty.push_back(p);
}

/******************************************
 | Function search                        |
 |                                        |
 | Begin to look for the two closest      |
 | points in the vector                   |
 ******************************************/
void PlusProches::search(){
    std::cout << "starting to search !" << std::endl;

    /* Sort both vectors, on by abscissa, one by ordinate */
    std::sort(Ty.begin(), Ty.end(), CompareY());
    std::sort(Tx.begin(), Tx.end(), CompareX());

    Paire near = divide(Tx.begin(), Tx.end(), Ty);
    std::cout << "Found nearest points : " << near.getP1().toString() << " : " << near.getP2().toString() << std::endl;
    std::cout << "Their euclidian distance is : " << near.getDistance() << std::endl;
}


/******************************************
 | Function readFile                      |
 |                                        |
 | Read a file where there are a list of  |
 | points                                 |
 ******************************************/
void PlusProches::readFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Erreur lors de l'ouverture du fichier " << filename << std::endl;
        return;
    }

    double x, y;
    while (file >> x >> y) {
        addPoint(x, y);
    }

    file.close();
}