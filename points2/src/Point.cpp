#include "../include/Point.hpp"
#include <sstream>


Point::Point(double x, double y)
: x(x), y(y){}

std::string Point::toString() const{
    std::ostringstream o;
    o << "[" << x << "][" << y << "]";
    return o.str();
}