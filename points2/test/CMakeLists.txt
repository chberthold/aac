include_directories("${PROJECT_SOURCE_DIR}")

macro(add_test_code X SRC)
    add_executable(${X} main.cpp ${SRC})
    target_link_libraries(${X} gtest gmock_main ${PROJECT_NAME})
    add_test(NAME ${X}-runner COMMAND ${X})
endmacro()

set(X ${PROJECT_NAME}-point)
add_test_code(${X} t_Point.cpp)

set(X ${PROJECT_NAME}-paire)
add_test_code(${X} t_Paire.cpp)
