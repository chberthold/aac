#include <gtest/gtest.h>

#include "Paire.hpp"

TEST(t_Paire, get_p1) {
    Point p1(10, 5);
    Point p2(2, 24);
    Paire p(p1, p2, 100.0);
    ASSERT_EQ(p.getP1().getX(), 10);
    ASSERT_EQ(p.getP1().getY(), 5);
}

TEST(t_Paire, get_p2) {
    Point p1(10, 5);
    Point p2(2, 24);
    Paire p(p1, p2, 120.0);
    ASSERT_EQ(p.getP2().getX(), 2);
    ASSERT_EQ(p.getP2().getY(), 24);
}

TEST(t_Paire, get_distance) {
    Point p1(10, 5);
    Point p2(2, 24);
    double distance = 98.5;
    Paire p(p1, p2, distance);
    ASSERT_EQ(p.getDistance(), 98.5);
}
