#include <gtest/gtest.h>

#include "Point.hpp"

TEST(t_Point, get_x) {
  Point p(10, 5);
  ASSERT_EQ(p.getX(), 10);
}

TEST(t_Point, get_y) {
  Point p(1, 3);
  ASSERT_EQ(p.getY(), 3);
}
