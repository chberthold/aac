// Guardians
#ifndef _EULERIAN_HPP_
#define _EULERIAN_HPP_

// Headers
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>

// Classe
class EulerianCycleFinder
{
public:
    using EdgeVector = std::vector<std::vector<std::string>>;
    struct Vertex { std::unordered_set<std::string> outgoingEdges;
                    std::unordered_set<std::string> ingoingEdges; };
    // Object Constructor
    explicit EulerianCycleFinder(const EdgeVector& edges)
    {
        ConstructMapping(edges);
    }
    bool IsEulerianCycle() const;
    bool FindEulerianCycle(std::string, std::vector<std::string>&);

private:
    std::unordered_map<std::string, Vertex> m_vertexMap;
    void ConstructMapping(const EdgeVector&);
    void DFS(std::string, std::vector<std::string>&);
};

// End
#endif
