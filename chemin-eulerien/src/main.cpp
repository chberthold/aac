#include <iostream>
#include "EulerianCycleFinder.hpp"

int main()
{
    EulerianCycleFinder::EdgeVector directedEdges;

    directedEdges.push_back({ "Rome", "Istanbul" });
    directedEdges.push_back({ "Istanbul", "Rome" });
    directedEdges.push_back({ "Berlin", "Istanbul" });
    directedEdges.push_back({ "Istanbul", "Berlin" });
    directedEdges.push_back({ "Paris", "Istanbul" });
    directedEdges.push_back({ "Istanbul", "Paris" });
    directedEdges.push_back({ "Madrid", "Istanbul" });
    directedEdges.push_back({ "Istanbul", "Madrid" });
    directedEdges.push_back({ "Rome", "Berlin" });
    directedEdges.push_back({ "Berlin", "Rome" });
    directedEdges.push_back({ "Berlin", "Paris" });
    directedEdges.push_back({ "Paris", "Berlin" });
    directedEdges.push_back({ "London", "Berlin" });
    directedEdges.push_back({ "Berlin", "London" });
    directedEdges.push_back({ "London", "Paris" });
    directedEdges.push_back({ "Paris", "London" });
    directedEdges.push_back({ "London", "Madrid" });
    directedEdges.push_back({ "Madrid", "London" });
    directedEdges.push_back({ "Madrid", "Paris" });
    directedEdges.push_back({ "Paris", "Madrid" });

    EulerianCycleFinder finder(directedEdges);

    if (finder.IsEulerianCycle())
    {
        std::vector<std::string> eulerianCycle;

        finder.FindEulerianCycle("Istanbul", eulerianCycle);

        std::cout << "Start --> ";

        // (4) The stack holds the complete Eulerian Cycle
        for (const auto& vertex : eulerianCycle)
        {
            std::cout << vertex << " --> ";
        }
        std::cout << "End";
    }
    else
    {
        std::cout << "Graph has no Eulerian Cycle!" << std::endl;
    }

    return 0;
}
