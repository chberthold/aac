#include "EulerianCycleFinder.hpp"

// For a directed graph: every vertex should have equal in-degree and out-degree edges
bool EulerianCycleFinder::IsEulerianCycle() const
{
    for (const auto& vertex : m_vertexMap)
    {
        if (vertex.second.ingoingEdges.size() !=
            vertex.second.outgoingEdges.size())
        {
            return false;
        }
    }
    return true;
}

// Hierholzer Algorithm: (1) -> (4)
bool EulerianCycleFinder::FindEulerianCycle(
    std::string startingVertex,
    std::vector<std::string>& eulerianCycle)
{
    // (1) Can choose any arbitrary vertex as a starting point
    DFS(startingVertex, eulerianCycle);
    return true;
}

// "Real" Constructor
void EulerianCycleFinder::ConstructMapping(const EdgeVector& edges)
{
    for (const auto& edge : edges)
    {
        std::string src{ edge.at(0) };
        std::string dest{ edge.at(1) };

        if (m_vertexMap.find(src) == m_vertexMap.end())
            m_vertexMap[src] = Vertex{};

        if (m_vertexMap.find(dest) == m_vertexMap.end())
            m_vertexMap[dest] = Vertex{};

        m_vertexMap[src].outgoingEdges.insert(dest);
        m_vertexMap[dest].ingoingEdges.insert(src);
    }
}

// Depth First Traversal
void EulerianCycleFinder::DFS(std::string src, std::vector<std::string>& eulerianCycle)
{
    Vertex& currVertex{ m_vertexMap[src] };

    // (2) Follow the outgoing edges of the vertex that we haven’t followed before and traverse the vertices
    while (!currVertex.outgoingEdges.empty())
    {
        std::string dest{ *currVertex.outgoingEdges.begin() };
        currVertex.outgoingEdges.erase(dest);

        Vertex& destVertex{ m_vertexMap[dest] };
        destVertex.ingoingEdges.erase(src);

        DFS(dest, eulerianCycle);
    }

    // (3) Push the vertex that we stuck to the top of the stack data structure which holds the Eulerian Cycle
    eulerianCycle.insert(eulerianCycle.begin(), src);
}
