#include "Node.hpp"
#include <iostream>
#include <sstream>

#define COUNT 10

Node::~Node(){
    if(left != nullptr){
        delete left;
    }
    if(right != nullptr){
        delete right;
    }
}


std::string Node::toString(int space) const
{
    std::ostringstream s;
    space += COUNT;
 
    if(this->right != nullptr){
        s << this->right->toString(space);
    }
 
    s << std::endl;
    for (int i = COUNT; i < space; i++)
        s << " ";
    s << this->caractere << " : " << this->nombreApparitions << std::endl;
 
    if(this->left != nullptr){
        s << this->left->toString(space);
    }

    return s.str();
}
 