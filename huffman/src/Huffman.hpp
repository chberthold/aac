#ifndef HUFFMAN_H
#define HUFFMAN_H
#include "Node.hpp"
#include <functional>
#include <ostream>
#include <string>
#include <map>

class Huffman{
    private:
        Node * root;
        std::map<char, std::string> huffmanCode;
    public:
        ~Huffman();
        Huffman();
        Huffman(std::string);
        void createTreeSemi(std::string fileName);
        std::string codeSemi(std::string fileName);
        std::string decodeSemi(std::string);
        void writeHuffmanTree(std::ostream &);
        std::string codeChar(char) const;
        void writeHuffmanMapInFile(std::string);
        void readHuffmanMapFromFile(std::string);
};

void writeSaveFile(std::string fileName, std::string save);
std::string readSaveFile(std::string);

#endif