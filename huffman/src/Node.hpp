#ifndef NODE_H
#define NODE_H

#include <string>

class Node{
    private:
        char caractere;
        int nombreApparitions;
        Node* left;
        Node* right;
    public:
        Node(char c) : caractere(c), nombreApparitions(1), left(nullptr), right(nullptr) {}
        ~Node();
        const Node* getLeft() const{return left;};
        const Node* getRight() const{return right;};
        void setLeft(Node* l){left = l;};
        void setRight(Node* r){right = r;};
        int getNombreApparitions() const{return nombreApparitions;};
        bool operator<(const Node& other) const {
            return nombreApparitions > other.nombreApparitions;
        }
        char getCaractere() const{return caractere;};
        void setNombreApparitions(int n){nombreApparitions = n;}
        void incrementerNombreApparitions(){nombreApparitions ++;}
        std::string toString(int = 0) const;
};

class NodeComparator{
    public:
        bool operator()(Node* n1, Node* n2) const{
            return n1->getNombreApparitions() > n2->getNombreApparitions();
        }
};

#endif