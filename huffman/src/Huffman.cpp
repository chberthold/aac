#include "Huffman.hpp"
#include "Node.hpp"
#include <cstdio>
#include <iterator>
#include <sstream>
#include <tuple>
#include <queue>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

Huffman::Huffman()
: root(nullptr){}

Huffman::Huffman(std::string fileName)
{ createTreeSemi(fileName); }

Huffman::~Huffman(){
    delete root;
}


/* Map the characters to the corresponding binary code */
void fillHuffmanMap(const Node* node, std::map<char, std::string>& huffmanMap, std::string code = "") {
    if (node != nullptr) {
        if (!node->getLeft() && !node->getRight()) {
            huffmanMap[node->getCaractere()] = code;
        }
        fillHuffmanMap(node->getLeft(), huffmanMap, code + "0");
        fillHuffmanMap(node->getRight(), huffmanMap, code + "1");
    }
}


/* Write a string in a file */
void writeSaveFile(std::string fileName, std::string save){
    std::ofstream outFile(fileName);

    if (outFile.is_open()) {
        outFile << save;
    }

    else{
        std::cerr << "Erreur lors de l'ouverture du fichier " << fileName << std::endl;
    }
}


/* Read the content of a file in a string */
std::string readSaveFile(std::string fileName){
    std::ifstream inFile(fileName);
    std::string code;
    std::string line;

    if (inFile.is_open()) {
        while (std::getline(inFile, line)) {
            code += line;
        }

        std::cout << "Code récupéré !" << std::endl;
        inFile.close();
    }
    else{
        std::cerr << "Erreur lors de l'ouverture du fichier " << fileName << std::endl;
    }

    return code;
}

/* Write the mapping of character -> binary code into a file */
void Huffman::writeHuffmanMapInFile(std::string fileName){
    std::ofstream outFile(fileName);

    if (outFile.is_open()) {

        for (std::map<char, std::string>::const_iterator it = huffmanCode.begin(); it != huffmanCode.end(); ++it) {

            outFile << it->first << " " << it->second << std::endl;
        }

        std::cout << "Arbre de Huffman enregistré dans " << fileName << std::endl;

        outFile.close();
    }
    else{
        std::cerr << "Erreur lors de l'ouverture du fichier " << fileName << std::endl;
    }
}

/* Read a file to retrieve the character->binary code couples  */
void Huffman::readHuffmanMapFromFile(std::string fileName){
    std::ifstream inFile(fileName);
    std::string value;
    std::string line;

    if (inFile.is_open()) {
        while (std::getline(inFile, line)) {
            huffmanCode[line.at(0)] = line.substr(2);
        }

        std::cout << "Arbre de Huffman récupéré !" << std::endl;
        inFile.close();
    }
    else{
        std::cerr << "Erreur lors de l'ouverture du fichier " << fileName << std::endl;
    }
}

/* Create a Huffman tree with the content of a file
   Uses a priority queue to sort by the number of times
   the char is in the file.
*/
void Huffman::createTreeSemi(std::string fileName){
    
    std::ifstream f(fileName);
    Node* left;
    Node* right;
    Node* parent;
    char caractere;
    std::priority_queue<Node*, std::vector<Node*>, NodeComparator> occurrences;
    Node* tempNode;
    bool exists;
    std::priority_queue<Node*, std::vector<Node*>, NodeComparator> tempQueue;


    if (f.is_open()) {

        /* Read all chars and count the times they appear in the file */
        while (f.get(caractere)) {
            tempNode = new Node(caractere);
            exists = false;
            
            /* Look into all the chars that have already been seen */
            while (!occurrences.empty()) {
                Node* currentNode = occurrences.top();
                occurrences.pop();

                /* If the character read has already been encountered */
                if (currentNode->getCaractere() == caractere) {
                    exists = true;
                    currentNode->incrementerNombreApparitions();
                }

                tempQueue.push(currentNode);
            }

            /* Put everything back into occurrences */
            while (!tempQueue.empty()) {
                occurrences.push(tempQueue.top());
                tempQueue.pop();
            }

            /* If the char has not been encountered, use the new node */
            if (!exists) {
                occurrences.push(tempNode);
            }
            else{
                delete tempNode;
            }
        }
        f.close();
        
        /* Create the Huffman tree with all the characters found in the file */
        while (occurrences.size() > 1) {
            left = occurrences.top();
            occurrences.pop();
            right = occurrences.top();
            occurrences.pop();

            parent = new Node('\0');
            parent->setNombreApparitions(left->getNombreApparitions() + right->getNombreApparitions());
            parent->setLeft(left);
            parent->setRight(right);

            occurrences.push(parent);
        }
        root = occurrences.top();
        //root = top;

        fillHuffmanMap(root, huffmanCode);
        
    }

    else std::cerr << "Erreur d'ouverture du fichier." << std::endl;
}

/* Code the content in the specified file in binary
    Uses the Huffman tree that has been created when first reading
    the file
*/
std::string Huffman::codeSemi(std::string fileName){
    std::ostringstream s;
    std::ifstream f(fileName);
    char caractere;

    if(f.is_open()){

        while (f.get(caractere)) {
            s << huffmanCode[caractere] << " ";
        }

    }

    else{
        std::cerr << "Erreur d'ouverture du fichier." << std::endl;
    }

    return s.str();
}

/* Translate the binary string provided into characters
    using the Huffman tree stored
*/
std::string Huffman::decodeSemi(std::string code){
    std::ostringstream decodedString;
    std::string codedChar;
    
    // Split the code, divider space
    std::stringstream splitString(code);  

    // While there is a coded character to decode
    while (splitString >> codedChar) {
        // Find the character linked to the binary code
        for (std::map<char, std::string>::const_iterator it = huffmanCode.begin(); it != huffmanCode.end(); ++it) {
            if (it->second == codedChar) {
                //std::cout << it->second << " \"" << it->first << "\"" << std::endl;
                decodedString << it->first;
                break;
            }
        }
    }

    return decodedString.str();
}

void Huffman::writeHuffmanTree(std::ostream & o){
    o << root->toString();
}