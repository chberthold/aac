#include "Huffman.hpp"
#include <iostream>
#include <fstream>
#include <cstring>


int main(int argc, char *argv[]){
    Huffman h;
    std::string fileName;
    std::string userInput;
    std::string codeFile;
    std::string code;
    
    if(argc >= 3){
        fileName = argv[2];
        if(std::strcmp(argv[1], "code") == 0){
            h.createTreeSemi(fileName);
            h.writeHuffmanTree(std::cout);
            code = h.codeSemi(fileName);

            std::cout << "Arbre créé, sauvegarder ? (y/n) ";
            std::getline(std::cin, userInput);

            if(userInput == "y"){
                std::cout << "Nom du fichier de sauvegarde : ";
                std::getline(std::cin, userInput);

                h.writeHuffmanMapInFile("../files/" + userInput);
            }

            std::cout << "Code pour le fichier : " << code << std::endl;

            std::cout << "Sauvegarder le code ? (y/n) ";
            std::getline(std::cin, userInput);

            if(userInput == "y"){
                std::cout << "Nom du fichier de sauvegarde du code : ";
                std::getline(std::cin, userInput);
            
                writeSaveFile("../files/" + userInput, code);
            }
            
        }
        // decode nom_fichier_map nom_fichier_code
        else{
            if(argc > 3){
                h.readHuffmanMapFromFile(fileName);
                code = readSaveFile(argv[3]);
                std::cout << "Phrase à partir du code " << code << " : " << h.decodeSemi(code) << std::endl;
            }
        }
    }

    else{
        std::cout << "Utiliser le programme :" << std::endl;
        std::cout << "./huffman code [fichier à coder]" << std::endl;
        std::cout << "OR" << std::endl;
        std::cout << "./huffman decode [fichier pour le codage] [fichier à décoder]" << std::endl;
    }

    return 0;
}