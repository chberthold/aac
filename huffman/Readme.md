# Algorithme 1 Chloé : codage de Huffman

## How to build

Dans le dossier build :

``` cmake .. ```
``` make ```

## How to use

Il faut d'abord coder le fichier avant de le décoder, en sauvegardant l'arbre créé lors de la première étape pour l'utiliser dans le décodage.

Coder un fichier texte :
``` ./huffman code  fichier```

Ce programme va créer un arbre de Huffman et proposer de le sauvegarder dans un fichier dans le dossier files. Pour décoder un fichier, il faudra utiliser ce fichier de sauvegarde de l'arbre pour avoir les correspondances.

Décoder un fichier texte (utiliser un fichier généré par la commande précédente):

``` ./huffman decode fichier_arbre fichier_a_decoder ```